package com.example.pouria.imdb;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;

public class IMDBActivity extends AppCompatActivity implements View.OnClickListener {
    EditText movie_name;
    Button search;
    public Context mContext=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imdb);
        Hawk.init(mContext).build();
        bindView();

    }


    private void bindView(){
        movie_name=(EditText)findViewById(R.id.movie_name);
        search=(Button)findViewById(R.id.search);
        search.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.search){
            String movie_namevalue=movie_name.getText().toString();
            Intent intent=new Intent(IMDBActivity.this,ShowActivity.class);
            intent.putExtra("movie_name",movie_namevalue);
            startActivity(intent);
        }
    }







}
