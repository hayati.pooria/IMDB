package com.example.pouria.imdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.*;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Key;

import cz.msebera.android.httpclient.Header;

public class ShowActivity extends AppCompatActivity {
    TextView result_1;
    TextView result_2;
    TextView result_3;
    TextView result_4;
    TextView result_5;
    TextView result_6;
    TextView result_7;
    TextView result_8;
    TextView result_9;
    TextView result_10;
    TextView result_11;
    ImageView poster;
    public Context mContext=this;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        Hawk.init(mContext).build();
        Intent receiveIntent=getIntent();
        String movie_name=receiveIntent.getStringExtra("movie_name");
        progressDialog=new ProgressDialog(mContext);
        progressDialog.setTitle("loading");
        progressDialog.setMessage("please wait to progress");
        bindView();
        search(movie_name);
    }

    private void bindView(){
     result_1=(TextView)findViewById(R.id.result_1);
        result_2=(TextView)findViewById(R.id.result_2);
        result_3=(TextView)findViewById(R.id.result_3);
        result_4=(TextView)findViewById(R.id.result_4);
        result_5=(TextView)findViewById(R.id.result_5);
        result_6=(TextView)findViewById(R.id.result_6);
        result_7=(TextView)findViewById(R.id.result_7);
        result_8=(TextView)findViewById(R.id.result_8);
        result_9=(TextView)findViewById(R.id.result_9);
        result_10=(TextView)findViewById(R.id.result_10);
        result_11=(TextView)findViewById(R.id.result_11);
        poster=(ImageView)findViewById(R.id.poster);

    }

    void search(String word){
        final String url="http://www.omdbapi.com/?t="+word+"&apikey=d8e640e2";
        final AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(2000);
        client.setConnectTimeout(2000);
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

               try {
                   showToast(mContext, throwable.toString());
                   client.setTimeout(1000);
                   parsRespond((String) Hawk.get(url));

               }catch (Exception e){
                   showToast(mContext, e.toString());
               }


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                AddOffline(url,responseString);

                parsRespond(responseString);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }
        });
    }
    void showToast(Context mContext,String text){
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    void parsRespond(String serverRespond){
        try {
            JSONObject serverObject=new JSONObject(serverRespond);
            String Title=serverObject.getString("Title");
            String Year=serverObject.getString("Year");
            String Released=serverObject.getString("Released");
            String Director=serverObject.getString("Director");
            String Actors=serverObject.getString("Actors");
            String Country=serverObject.getString("Country");
            String imdbRating=serverObject.getString("imdbRating");
            String Writer=serverObject.getString("Writer");
            String Genre=serverObject.getString("Genre");
            String Runtime=serverObject.getString("Runtime");
            String Plot=serverObject.getString("Plot");

            result_1.setText(String.format("title: %s", Title));
            result_2.setText(String.format("IMDB: %s", imdbRating));
            result_3.setText(String.format("Director: %s", Director));
            result_4.setText(String.format("Actors: %s", Actors));
            result_5.setText(String.format("Genre: %s", Genre));
            result_6.setText(String.format("Writer: %s", Writer));
            result_7.setText(String.format("Year: %s", Year));
            result_8.setText(String.format("Released: %s", Released));
            result_9.setText(String.format("Country: %s", Country));
            result_10.setText(String.format("Runtime: %s", Runtime));
            result_11.setText(String.format("Plot: %s", Plot));
            String posterURL=serverObject.getString("Poster");

            Picasso.get().load(posterURL).into(poster);



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void AddOffline(String key,String value ){
        Hawk.put(key,value);
    }
}
